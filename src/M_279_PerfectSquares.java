
/**
 *给定正整数 n，找到若干个完全平方数（比如 1, 4, 9, 16, ...）使得它们的和等于 n。
 * 你需要让组成和的完全平方数的个数最少。
 *
 * 示例 1:
 *
 * 输入: n = 12
 * 输出: 3
 * 解释: 12 = 4 + 4 + 4.
 * 示例 2:
 *
 * 输入: n = 13
 * 输出: 2
 * 解释: 13 = 4 + 9.
 *
 */
public class M_279_PerfectSquares {
    public int numSquares(int n) {
        if (n <= 3) {
            return n;
        }
        // dp[i]表示i对应的最小完全平方数的个数
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        dp[3] = 3;

        int i;
        int j;
        int min;
        int tmp;

        for (i = 4;  i < n + 1; i++) {
            tmp = (int) Math.sqrt(i);
            if (tmp * tmp == i) { // 如果本身就是完全平方数，则直接为1，继续下一个数的计算
                dp[i] = 1;
                continue;
            } else { // 否则将min 默认设置为 dp[i -1] + dp[1]
                min = dp[i - 1] + dp[1];
            }

           for (j = 1; j <= i / 2; j++) { // 循环遍历 i/2 ，找到最小的完全平方数个数
               min = Math.min(dp[j] + dp[i - j], min);
               if (min == 2) { // 优化项，如果恰好找到两个完全平方数，也直接跳出本次循环，继续查找别的
                   dp[i] = 2;
                   break;
               }
           }
           dp[i] = min;
        }

        return dp[n];
    }
}
